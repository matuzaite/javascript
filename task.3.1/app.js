const registerBtn = document.getElementById("register-button");
const formContainer = document.querySelector(".registration-container");
const form = document.querySelector("form");
const closeFormBtn = document.getElementById("close-registration-button");
const submitBtn = document.querySelector(".submit-button");
const confirmPassword = document.getElementById("confirm-password");
const facebookBtn = document.querySelector(".facebook-button");

registerBtn.addEventListener("click", () => {
  formContainer.style.display = "flex";
});

closeFormBtn.addEventListener("click", () => {
  formContainer.style.display = "none";
});

//setting local storage
const submitRegistration = (e) => {
  e.preventDefault();

  //checking if passwords match
  let password = document.getElementById("password");
  if (password.value === confirmPassword.value) {
    let name = document.getElementById("name").value;
    let email = document.getElementById("email").value;
    password = password.value;
    const user = { name: name, email: email, password: password };

    //checking if there are some users already
    let users;
    if (localStorage.getItem("users") === null) {
      users = [];
    } else {
      users = JSON.parse(localStorage.getItem("users"));
    }
    users.push(user);
    localStorage.setItem("users", JSON.stringify(users));
    alert("Success!");

    //console.log users
    console.log(users);
    //console.log new user
    console.log(user);

    //closing registration popup
    formContainer.style.display = "none";

    //clearing inputs
    form.reset();
  } else {
    alert("Passwords don't match! Try again.");
  }
};

submitBtn.addEventListener("click", submitRegistration);

//Facebook login
// const facebookCall = FB.login(
//   function (response) {
//     console.log(response);
//   },
//   { scope: "public_profile,email" }
// );

// window.fbAsyncInit = function () {
//   FB.init({
//     appId: "my_e_store",
//     xfbml: true,
//     version: "v12.0",
//   });
//   FB.AppEvents.logPageView();
// };

// (function (d, s, id) {
//   var js,
//     fjs = d.getElementsByTagName(s)[0];
//   if (d.getElementById(id)) {
//     return;
//   }
//   js = d.createElement(s);
//   js.id = id;
//   js.src = "https://connect.facebook.net/en_US/sdk.js";
//   fjs.parentNode.insertBefore(js, fjs);
// })(document, "script", "facebook-jssdk");

// facebookBtn.addEventListener("click", () => {
//   FB.login(function (response) {
//     if (response.status === "connected") {
//       console.log("hoooray");
//     } else {
//       // The person is not logged into your webpage or we are unable to tell.
//       console.log("error");
//     }
//   });
// });
