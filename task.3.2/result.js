const result = document.querySelector(".result-container");
const forms = JSON.parse(localStorage.getItem("validForms"));

//if nothing to display
if (forms == null) {
  result.style.display = "none";
}

(function resultsReceived() {
  //creating table
  let table = document.createElement("table");
  result.appendChild(table);
  //table headers
  table.innerHTML = `<th>Buyer</th>
    <th>Purchase</th>
    <th>Billing Address</th>
    <th>Delivery Address</th>
    <th>ZIP-CODE</th>
    <th>Mobile Number</th>
    <th>Payment Details</th>
    `;

  let tbody = document.createElement("tbody");
  table.appendChild(tbody);

  for (let i = 0; i < forms.length; i++) {
    console.log(forms[i].fullName);
    const row = document.createElement("tr");
    tbody.appendChild(row);

    //data received
    row.innerHTML = `<td>${forms[i].fullName}</td>
        <td>${forms[i].product}</td>
        <td>${forms[i].billingAddress}</td>
        <td>${forms[i].deliveryAddress}</td>
        <td>${forms[i].zipCode}</td>
        <td>${forms[i].mobilePhone}</td>
        <td>${forms[i].cardNumber}</td>`;
  }
})();
