//order form selectors
const orderFormOpenBtn = document.getElementById("order-btn");
const closeOrderFormBtn = document.getElementById("close-order-form-btn");
const orderForm = document.querySelector(".order-container");
const formItself = document.querySelector("form");
const purchase = document.getElementById("model-select");
const comment = document.getElementById("comment");
const firstName = document.getElementById("name");
const lastName = document.getElementById("lastname");
const billAddress = document.getElementById("street");
const delAddress = document.getElementById("delivery-street");
const billState = document.getElementById("state-select");
const delState = document.getElementById("delivery-state-select");
const zipcode = document.getElementById("zip");
const phone = document.getElementById("phone");
const adressCheckbox = document.getElementById("same-address");
const cardType = document.getElementById("credit-type");
const cardNumber = document.getElementById("card-number");
const cardExDate = document.getElementById("exp-date");
const cvv = document.getElementById("cvv");
const orderBtn = document.querySelector(".order-button");
const reqName = document.getElementById("reqName");
const reqLastName = document.getElementById("reqLastName");
const reqStreet = document.getElementById("reqStreet");
const reqZipcode = document.getElementById("reqZipcode");
const reqPhone = document.getElementById("reqPhone");
const reqCardNumber = document.getElementById("reqCardNumber");
const reqCVV = document.getElementById("reqCVV");

//open form
orderFormOpenBtn.addEventListener("click", () => {
  orderForm.style.display = "flex";
});
//close form
closeOrderFormBtn.addEventListener("click", () => {
  orderForm.style.display = "none";
});

//error message
const showError = (element, message) => {
  document.getElementById(element).innerText = message;
};
//hidden error message
const hideError = (element) => {
  document.getElementById(element).innerText = "";
};
//not valid input error
const inputError = (element) => {
  element.className = "non-valid-class";
};
//valid input
const inputSuccess = (element) => {
  element.className = "valid-class";
};

//firstname
firstName.addEventListener("blur", () => {
  if (firstName.value == "") {
    inputError(firstName);
    showError("reqName", "Insert valid name");
  } else {
    inputSuccess(firstName);
    hideError("reqName");
  }
});

//lastname
lastName.addEventListener("blur", () => {
  if (lastName.value == "") {
    inputError(lastName);
    showError("reqLastName", "Insert valid last name");
  } else {
    inputSuccess(lastName);
    hideError("reqLastName");
  }
});

//address
billAddress.addEventListener("blur", () => {
  if (billAddress.value == "") {
    inputError(billAddress);
    showError("reqStreet", "Insert valid address");
  } else {
    inputSuccess(billAddress);
    hideError("reqStreet");
  }
});

//zipcode
zipcode.addEventListener("blur", () => {
  if (zipcode.value == "") {
    inputError(zipcode);
    showError("reqZipcode", "Insert valid zip-code");
  } else {
    inputSuccess(zipcode);
    hideError("reqZipcode");
  }
});

//mobile phone
phone.addEventListener("blur", () => {
  if (phone.value.length < 9) {
    inputError(phone);
    showError("reqPhone", "Minimum 9-digits");
  } else {
    inputSuccess(phone);
    hideError("reqPhone");
  }
});

//cardNumber
cardNumber.addEventListener("blur", () => {
  if (cardNumber.value.length !== 16) {
    inputError(cardNumber);
    showError("reqCardNumber", "16-digits");
  } else {
    inputSuccess(cardNumber);
    hideError("reqCardNumber");
  }
});

//cardNumber
cvv.addEventListener("blur", () => {
  if (cvv.value.length !== 3) {
    inputError(cvv);
    showError("reqCVV", "3-digits");
  } else {
    inputSuccess(cvv);
    hideError("reqCVV");
  }
});

//VALIDATION
let isValid;

//if valid send data
const finishOrderForm = (e) => {
  e.preventDefault();
  isValid = formItself.checkValidity();
  if (isValid) {
    let validForms = [];

    const validForm = {
      product: purchase.value,
      comment: comment.value,
      fullName: firstName.value + " " + lastName.value,
      billingAddress: `${billAddress.value} ${billState.value}`,
      deliveryAddress: `${delAddress.value} ${delState.value}`,
      zipCode: zipcode.value,
      mobilePhone: phone.value,
      cardIssuer: cardType.value,
      cardNumber: cardNumber.value,
      cardExpirationDate: cardExDate.value,
      CVV: cvv.value,
    };

    //checking local storage
    if (localStorage.getItem("validForms") === null) {
      validForms = [];
    } else {
      validForms = JSON.parse(localStorage.getItem("validForms"));
    }
    validForms.push(validForm);
    localStorage.setItem("validForms", JSON.stringify(validForms));
    alert("Success!");
    orderForm.style.display = "none";
    window.document.location = "./result.html";
  } else {
    alert("Please fill all theinput fields");
  }
};

orderBtn.addEventListener("click", finishOrderForm);

//references to the date dropdown's
const yearSelect = document.getElementById("year");
const monthSelect = document.getElementById("month");
const daySelect = document.getElementById("day");

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

//Months are always the same
(function populateMonths() {
  for (let i = 0; i < months.length; i++) {
    const option = document.createElement("option");
    option.textContent = months[i];
    monthSelect.appendChild(option);
  }
  monthSelect.value = "January";
})();

let previousDay;

function populateDays(month) {
  //Delete all of the children of the day dropdown
  //if they do exist
  while (daySelect.firstChild) {
    daySelect.removeChild(daySelect.firstChild);
  }
  //Holds the number of days in the month
  let dayNum;
  //Get the current year
  let year = yearSelect.value;

  if (
    month === "January" ||
    month === "March" ||
    month === "May" ||
    month === "July" ||
    month === "August" ||
    month === "October" ||
    month === "December"
  ) {
    dayNum = 31;
  } else if (
    month === "April" ||
    month === "June" ||
    month === "September" ||
    month === "November"
  ) {
    dayNum = 30;
  } else {
    //Check for a leap year
    if (new Date(year, 1, 29).getMonth() === 1) {
      dayNum = 29;
    } else {
      dayNum = 28;
    }
  }
  //Insert the correct days into the day <select>
  for (let i = 1; i <= dayNum; i++) {
    const option = document.createElement("option");
    option.textContent = i;
    daySelect.appendChild(option);
  }
  if (previousDay) {
    daySelect.value = previousDay;
    if (daySelect.value === "") {
      daySelect.value = previousDay - 1;
    }
    if (daySelect.value === "") {
      daySelect.value = previousDay - 2;
    }
    if (daySelect.value === "") {
      daySelect.value = previousDay - 3;
    }
  }
}

function populateYears() {
  //Get the current year as a number
  let year = new Date().getFullYear();
  //not earlier than current year
  for (let i = 0; i < 1; i++) {
    const option = document.createElement("option");
    option.textContent = year - i;
    yearSelect.appendChild(option);
  }
}

populateDays(monthSelect.value);
populateYears();

yearSelect.onchange = function () {
  populateDays(monthSelect.value);
};
monthSelect.onchange = function () {
  populateDays(monthSelect.value);
};
daySelect.onchange = function () {
  previousDay = daySelect.value;
};
