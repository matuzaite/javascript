const registerBtn = document.getElementById("register-button");
const formContainer = document.querySelector(".registration-container");
const form = document.querySelector("form");
const closeFormBtn = document.getElementById("close-registration-button");
const submitBtn = document.querySelector(".submit-button");
const confirmPassword = document.getElementById("reg-confirm-password");
const facebookBtn = document.querySelector(".facebook-button");

registerBtn.addEventListener("click", () => {
  formContainer.style.display = "flex";
});

closeFormBtn.addEventListener("click", () => {
  formContainer.style.display = "none";
});

//setting local storage
const submitRegistration = (e) => {
  e.preventDefault();

  //checking if passwords match
  let password = document.getElementById("reg-password");
  if (password.value === confirmPassword.value) {
    let name = document.getElementById("regName").value;
    let email = document.getElementById("email").value;
    password = password.value;
    const user = { name: name, email: email, password: password };

    //checking if there are some users already
    let users;
    if (localStorage.getItem("users") === null) {
      users = [];
    } else {
      users = JSON.parse(localStorage.getItem("users"));
    }
    users.push(user);
    localStorage.setItem("users", JSON.stringify(users));
    alert("Success!");

    //console.log users
    console.log(users);
    //console.log new user
    console.log(user);

    //closing registration popup
    formContainer.style.display = "none";

    //clearing inputs
    form.reset();
  } else {
    alert("Passwords don't match! Try again.");
  }
};

submitBtn.addEventListener("click", submitRegistration);
