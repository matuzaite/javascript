const headerArr = Array.from(document.getElementsByTagName("header"));

const sticky = (arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (window.pageYOffset === 0) {
      arr[i].className = "sticky";
    } else {
      arr[i].classList.remove("sticky");
    }
  }
};

window.addEventListener("scroll", sticky(headerArr), false);
