//selectors
const arrowNext = document.querySelector(".next");
const arrowPrev = document.querySelector(".prev");
const cards = document.querySelector(".card-pictures");
const cardsArr = Array.from(cards.children);
const bullets = document.querySelector(".bullets");
const bulletsArr = Array.from(bullets.children);
const slides = document.getElementsByClassName("card");
const slide1 = document.getElementById("slide-1");
const slide2 = document.getElementById("slide-2");
const slide3 = document.getElementById("slide-3");
const modal = document.querySelector(".modal");
const closeModalBtn = document.querySelector("#close-modal-btn");
const modalSlide = document.querySelector("#modal-pic");
const carouselContainer = document.querySelector(".carousel-container");
let autoplay;
const totalSlides = cardsArr.length;
const totalBullets = bulletsArr.length;
let slideIdx = 0;
let bulletIdx = 0;

const changeSlide = () => {
  for (let slide of slides) {
    slide.classList.remove("current-slide");
    slide.classList.add("hidden");
  }
  slides[slideIdx].classList.add("current-slide");
  slides[slideIdx].classList.remove("hidden");
};

const changeBullet = () => {
  for (let bullet of bulletsArr) {
    bullet.classList.remove("current-bullet");
  }
  bulletsArr[bulletIdx].classList.add("current-bullet");
};

const moveToNextSlide = () => {
  changeBullet();
  changeSlide();
  if (slideIdx === totalSlides - 1 && bulletIdx === totalBullets - 1) {
    slideIdx = 0;
    bulletIdx = 0;
  } else {
    slideIdx++;
    bulletIdx++;
  }
};

const moveToPrevSlide = () => {
  changeBullet();
  changeSlide();
  if (slideIdx === 0 && bulletIdx === 0) {
    slideIdx = totalSlides - 1;
    bulletIdx = totalBullets - 1;
  } else {
    slideIdx--;
    bulletIdx--;
  }
};

arrowNext.addEventListener("click", () => {
  clearInterval(autoplay);
  moveToNextSlide();
});

arrowPrev.addEventListener("click", () => {
  clearInterval(autoplay);
  moveToPrevSlide();
});

const modalDisplay = (slide) => {
  modal.style.display = "flex";
  carouselContainer.style.display = "none";
  modalSlide.src = slide.src;
};

slide1.addEventListener("click", () => {
  modalDisplay(slide1);
});

slide2.addEventListener("click", () => {
  modalDisplay(slide2);
});

slide3.addEventListener("click", () => {
  modalDisplay(slide3);
});

closeModalBtn.addEventListener("click", () => {
  carouselContainer.style.display = "flex";
  modal.style.display = "none";
});

document.addEventListener("DOMContentLoaded", () => {
  autoplay = setInterval(function () {
    moveToNextSlide();
  }, 3000);
});
