//selectors
const firstName = document.getElementById("firstname");
const lastName = document.getElementById("lastname");
const firstPurchaseDate = document.getElementById("first-purchase-date");
const nextPurchaseDate = document.getElementById("next-purchase-date");
const paymentMethod = document.getElementById("payment-method");
const discount = document.getElementById("discount");
const calculateBtn = document.getElementById("calculate-btn");
const inputs = document.querySelectorAll(".required-input");
const inputArr = Array.from(inputs);

//VALIDATION
let isFormValid;

function checkIfValid() {
  for (let i = 0; i < inputArr.length; i++) {
    if (inputArr[i].value == "") {
      isFormValid = false;
    } else {
      isFormValid = true;
    }
  }
  return isFormValid;
}

function alertInvalid(element) {
  if (element.value == "") {
    element.classList.add("invalid");
  } else {
    element.classList.add("valid");
  }
}

firstName.addEventListener("blur", () => {
  alertInvalid(firstName);
});
lastName.addEventListener("blur", () => {
  alertInvalid(lastName);
});
firstPurchaseDate.addEventListener("blur", () => {
  alertInvalid(firstPurchaseDate);
});
paymentMethod.addEventListener("blur", () => {
  alertInvalid(paymentMethod);
});

//FULLNAME OBJECT
class Buyer {
  constructor(firstname, lastname) {
    this.firstname = firstname.value;
    this.lastname = lastname.value;
  }
}

//ALL INFO OBJECT
class AllInfo {
  displayInfo(buyer, firstBuy, nextBuy, payment) {
    console.log(`First name : ${buyer.firstname}`);
    console.log(`Last name : ${buyer.lastname}`);
    console.log(`Date of first purchase : ${firstBuy.value}`);
    console.log(`Date of second purchase : ${nextBuy.value}`);
    console.log(`Payment method : ${payment.value}`);
  }
}

//DISCOUNT OBJECT
discount.innerText = "";
class CalculateDiscount {
  calculation(firstBuy, nextBuy, currentDiscount) {
    if (firstBuy.value !== "" && nextBuy.value !== "") {
      currentDiscount = 5;
    } else {
      currentDiscount = 0;
    }
    discount.innerText = `${currentDiscount}%`;
    console.log(`Discount received: ${currentDiscount}%`);
  }
}

calculateBtn.addEventListener("click", (e) => {
  e.preventDefault();
  checkIfValid();

  if (isFormValid) {
    const newBuyer = new Buyer(firstName, lastName);
    const allInfo = new AllInfo();
    allInfo.displayInfo(
      newBuyer,
      firstPurchaseDate,
      nextPurchaseDate,
      paymentMethod
    );

    const possibleDiscount = new CalculateDiscount();
    possibleDiscount.calculation(firstPurchaseDate, nextPurchaseDate, discount);
  } else {
    alert("Please fill all the fields!");
  }
});
