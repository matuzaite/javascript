let user = JSON.parse(localStorage.getItem("users"));

const displayResults = () => {
  const resultsDiv = document.querySelector(".result");

  resultsDiv.innerHTML = `<h2>Congratulations, ${user.username}</h2>
  <ul>
    <li>E-mail: ${user.email}</li>
    <li>Username: ${user.username}</li>
    <li class='lodgins'><hr>Preffered lodgins:
    <i>${user.prefferedLodgins}</i></li>
  </ul>`;
};

displayResults();
