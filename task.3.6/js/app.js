//selectors
const username = document.getElementById("username");
const password = document.getElementById("password");
const passwordConfirm = document.getElementById("password-confirm");
const email = document.getElementById("email");
const btn = document.querySelector(".reg-btn");
const checkBoxes = document.getElementsByTagName("input");
const emailReg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const passReg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

//checking reg and passwords
let isValid;
const checkReg = (el, reg) => {
  if (password.value !== passwordConfirm.value) {
    alert("Passwords must match!");
    isValid = false;
  }
  if (reg.test(el.value) == false) {
    alert(`Invalid ${el.name}`);
    isValid = false;
    el.style.border = "2px red solid";
  } else {
    el.style.border = "2px green solid";
    isValid = true;
  }
  return isValid;
};
//submit form
const submitForm = () => {
  checkReg(email, emailReg);
  checkReg(password, passReg);
  let newArr = [];
  if (isValid) {
    for (let i = 0; i < checkBoxes.length; i++) {
      if (checkBoxes[i].checked) {
        newArr.push(checkBoxes[i].value);
      }
    }

    const user = {
      username: username.value,
      email: email.value,
      prefferedLodgins: [...newArr],
    };
    localStorage.setItem("users", JSON.stringify(user));
    console.log(user);
    window.location = "/account.html";
  }
};

btn.addEventListener("click", (e) => {
  e.preventDefault();
  submitForm();
});
